
#include "Instruction.h"

Instruction::Instruction(Func f, const char* mnemonic, uint8_t length, uint8_t cycles) : func(f), _mnemonic(mnemonic),
																						 length(length), cycles(cycles)
{}

Instruction::Instruction()
{}

string Instruction::getMnemonic(uint16_t operand) const
{
	if (length <= 2)
		operand &= 0x00FF;

	const auto size = std::snprintf(nullptr, 0, _mnemonic.c_str(), operand) + 1;

	std::unique_ptr<char> buffer(new char[size]);

	std::sprintf(buffer.get(), _mnemonic.c_str(), operand);

	return string(buffer.get());
}
