
#include "utils.h"
#include "Registers.h"

void utils::subtract(Registers& reg, uint8_t* dst, uint8_t x, bool carry)
{
	const uint16_t result = *dst - x - carry;

	reg.setSR(Carry, (result & 0x100) != 0);
	reg.setSR(Auxiliary, (~(*dst ^ result ^ x) & 0x10) != 0);
	reg.setSR(result, Sign, Zero, Parity);

	*dst = static_cast<uint8_t>(result & 0xFF);
}

void utils::compare(Registers& reg, uint8_t x, uint8_t y)
{
	const uint16_t result = x - y;

	reg.setSR(Carry, (result & 0x100) != 0);
	reg.setSR(Auxiliary, (~(x ^ result ^ y) & 0x10) != 0);
	reg.setSR(result, Sign, Zero, Parity);
}

void utils::add(Registers& reg, uint8_t* dst, uint8_t x, bool carry)
{
	const uint16_t result = *dst + x + carry;

	reg.setSR(Carry, (result & 0x100) != 0);
	reg.setSR(Auxiliary, ((*dst ^ result ^ x) & 0x10) != 0);
	reg.setSR(result, Sign, Zero, Parity);

	*dst = static_cast<uint8_t>(result & 0xFF);
}

bool utils::parity(uint8_t x)
{
	uint8_t bits = 0;

	for (int i = 0; i < 8; i++)
		bits += ((x >> i) & 1);

	return (bits & 1) == 0;
}
