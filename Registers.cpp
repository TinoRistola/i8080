
#include "Registers.h"

Registers::Registers()
{
	reset();
}

void Registers::reset()
{
	setSR(0);

	PC = 0;
	SP = 0;

	A = B = C = D = E = H = L = (uint8_t)0;
}

uint8_t Registers::getSR() const
{
	uint8_t result = defaultValue;

	result |= SR[Carry] << Carry;
	result |= SR[Parity] << Parity;
	result |= SR[Auxiliary] << Auxiliary;
	result |= SR[Zero] << Zero;
	result |= SR[Sign] << Sign;

	return result;
}

uint16_t Registers::getPair(enumRegisterPairs pair) const
{
	switch (pair)
	{
		case enumRegisterPairs::B:
			return static_cast<uint16_t>((B << 8) | C);
		case enumRegisterPairs::D:
			return static_cast<uint16_t>((D << 8) | E);
		case enumRegisterPairs::H:
			return static_cast<uint16_t>((H << 8) | L);
		case enumRegisterPairs::PSW:
			return static_cast<uint16_t>((A << 8) | getSR());
		default:
			return 0;
	}
}

void Registers::setPair(enumRegisterPairs pair, uint16_t value)
{
	const auto hi = static_cast<uint8_t>(value >> 8);
	const auto lo = static_cast<uint8_t>(value & 0xFF);

	switch (pair)
	{
		case enumRegisterPairs::B:
			B = hi;
			C = lo;
			break;
		case enumRegisterPairs::D:
			D = hi;
			E = lo;
			break;
		case enumRegisterPairs::H:
			H = hi;
			L = lo;
			break;
		case enumRegisterPairs::PSW:
			A = hi;
			setSR(lo);
			break;
	}
}

void Registers::setSR(uint8_t value)
{
	SR[Carry] = ((value >> Carry) & 1) != 0;
	SR[Parity] = ((value >> Parity) & 1) != 0;
	SR[Auxiliary] = ((value >> Auxiliary) & 1) != 0;
	SR[Zero] = ((value >> Zero) & 1) != 0;
	SR[Sign] = ((value >> Sign) & 1) != 0;
}

uint8_t* Registers::getRegister(uint8_t reg)
{
	switch (reg)
	{
		case 0b000:
			return &B;
		case 0b001:
			return &C;
		case 0b010:
			return &D;
		case 0b011:
			return &E;
		case 0b100:
			return &H;
		case 0b101:
			return &L;
		case 0b111:
			return &A;
		default:
			return nullptr;
	}
}

void Registers::setSR(uint16_t result, enumRegisterFlags flag)
{
	switch (flag)
	{
		case enumRegisterFlags::Carry:
			setSR(flag, (result & 0x100) != 0);
			break;
		case enumRegisterFlags::Parity:
			setSR(flag, utils::parity(result));
			break;
		case enumRegisterFlags::Auxiliary:
			setSR(flag, ((result & 0b11110000) >> 4) > 0);
			break;
		case enumRegisterFlags::Zero:
			setSR(flag, (result & 0xFF) == 0);
			break;
		case enumRegisterFlags::Sign:
			setSR(flag, (result & 0x80) != 0);
			break;
	}
}
