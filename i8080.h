
#ifndef I8080_I8080_H
#define I8080_I8080_H

#include <iostream>
#include <fstream>
#include <array>

#include "Registers.h"
#include "Memory.hpp"
#include "Instruction.h"
#include "utils.h"
#include "InputOutput.h"

class i8080
{
	uint8_t _cycles;
	uint8_t _currentOpcode;

	std::array<Instruction, 0x100> _instr;

	bool _enableInterrupts;

public:
	i8080();
	Registers reg;
	Memory<0x10000> ram;
	InputOutput io;

	void reset();
	void step();
	void interrupt(uint16_t addr);
	void load(const char* fileName, uint16_t offset);

	void push16(uint16_t value);

	uint16_t pop16();

private:
	uint8_t* getDST();
	uint8_t* getSRC();
	uint8_t* getRegister(uint8_t _reg);

	void call(uint16_t operand);
	void ret();

	void ACI(uint16_t operand);
	void ADC(uint16_t operand);
	void ADD(uint16_t operand);
	void ADI(uint16_t operand);
	void ANA(uint16_t operand);
	void ANI(uint16_t operand);
	void CALL(uint16_t operand);
	void CC(uint16_t operand);
	void CM(uint16_t operand);
	void CMA(uint16_t operand);
	void CMC(uint16_t operand);
	void CMP(uint16_t operand);
	void CNC(uint16_t operand);
	void CNZ(uint16_t operand);
	void CP(uint16_t operand);
	void CPE(uint16_t operand);
	void CPI(uint16_t operand);
	void CPO(uint16_t operand);
	void CZ(uint16_t operand);
	void DAA(uint16_t operand);
	void DAD(uint16_t operand);
	void DCR(uint16_t operand);
	void DCX(uint16_t operand);
	void DI(uint16_t operand);
	void EI(uint16_t operand);
	void HLT(uint16_t operand);
	void IN(uint16_t operand);
	void INR(uint16_t operand);
	void INX(uint16_t operand);
	void JC(uint16_t operand);
	void JM(uint16_t operand);
	void JMP(uint16_t operand);
	void JNC(uint16_t operand);
	void JNZ(uint16_t operand);
	void JP(uint16_t operand);
	void JPE(uint16_t operand);
	void JPO(uint16_t operand);
	void JZ(uint16_t operand);
	void LDA(uint16_t operand);
	void LDAX(uint16_t operand);
	void LHLD(uint16_t operand);
	void LXI(uint16_t operand);
	void MOV(uint16_t operand);
	void MVI(uint16_t operand);
	void NOP(uint16_t operand);
	void ORA(uint16_t operand);
	void ORI(uint16_t operand);
	void OUT(uint16_t operand);
	void PCHL(uint16_t operand);
	void POP(uint16_t operand);
	void PUSH(uint16_t operand);
	void RAL(uint16_t operand);
	void RAR(uint16_t operand);
	void RC(uint16_t operand);
	void RET(uint16_t operand);
	void RLC(uint16_t operand);
	void RM(uint16_t operand);
	void RNC(uint16_t operand);
	void RNZ(uint16_t operand);
	void RP(uint16_t operand);
	void RPE(uint16_t operand);
	void RPO(uint16_t operand);
	void RRC(uint16_t operand);
	void RST(uint16_t operand);
	void RZ(uint16_t operand);
	void SBB(uint16_t operand);
	void SBI(uint16_t operand);
	void SHLD(uint16_t operand);
	void SPHL(uint16_t operand);
	void STA(uint16_t operand);
	void STAX(uint16_t operand);
	void STC(uint16_t operand);
	void SUB(uint16_t operand);
	void SUI(uint16_t operand);
	void XCHG(uint16_t operand);
	void XRA(uint16_t operand);
	void XRI(uint16_t operand);
	void XTHL(uint16_t operand);
};


#endif //I8080_I8080_H
