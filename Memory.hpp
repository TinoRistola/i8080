
#ifndef I8080_MEMORY_H
#define I8080_MEMORY_H

#include <cstdint>

template<std::size_t size>
class Memory
{
	uint8_t _data[size];

public:
	inline uint8_t read8(uint16_t addr) const
	{
		return _data[addr];
	}

	inline uint8_t* read8ptr(uint16_t addr)
	{
		return &_data[addr];
	}

	uint16_t read16(uint16_t addr) const
	{
		return (read8(addr + 1) << 8) | read8(addr);
	}

	inline uint8_t* get() { return _data; }

	inline void write8(uint16_t addr, uint8_t value)
	{
		_data[addr] = value;
	}

	void write16(uint16_t addr, uint16_t value)
	{
		_data[addr]     = static_cast<uint8_t>(value & 0xFF);
		_data[addr + 1] = static_cast<uint8_t>(value >> 8);
	}
};

#endif //I8080_MEMORY_H
