
#include "InputOutput.h"

void InputOutput::write8input(uint8_t port, uint8_t value)
{
	_input[port] = value;
}

void InputOutput::write8output(uint8_t port, uint8_t value)
{
	_output[port] = value;
}

uint8_t InputOutput::read8input(uint8_t port)
{
	return _input[port];
}

uint8_t InputOutput::read8output(uint8_t port)
{
	return _output[port];
}
