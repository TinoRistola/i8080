
#ifndef I8080_REGISTERS_H
#define I8080_REGISTERS_H

#include <cstdint>

#include "utils.h"

enum enumRegisterFlags : std::size_t
{
	Carry     = 0,
	Parity    = 2,
	Auxiliary = 4,
	Zero      = 6,
	Sign      = 7
};

enum enumRegisterPairs
{
	B   = 0b00,
	D   = 0b01,
	H   = 0b10,
	PSW = 0b11
};

class Registers
{
	static constexpr uint8_t defaultValue = 0b00000010;
public:
	Registers();
	uint8_t A, B, C, D, E, H, L;

	uint16_t SP, PC;

	bool SR[8];

	void reset();

	uint8_t getSR() const;
	inline bool getSR(enumRegisterFlags flag) const { return SR[flag]; }

	uint16_t getPair(enumRegisterPairs pair) const;
	void setPair(enumRegisterPairs pair, uint16_t value);

	uint8_t* getRegister(uint8_t reg);

	void setSR(uint8_t value);
	inline void setSR(enumRegisterFlags flag, bool value) { SR[flag] = value; }

	void setSR(uint16_t result, enumRegisterFlags flag);

	template<typename... Flags>
	void setSR(uint16_t result, enumRegisterFlags flag, Flags... flags)
	{
		setSR(result, flag);
		setSR(result, flags...);
	}
};

#endif //I8080_REGISTERS_H
