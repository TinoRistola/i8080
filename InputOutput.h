
#ifndef I8080_INPUTOUTPUT_H
#define I8080_INPUTOUTPUT_H

#include <cstdint>
#include <array>

class InputOutput
{
	std::array<uint8_t, 0x100> _output;
	std::array<uint8_t, 0x100> _input;

public:
	void write8input(uint8_t port, uint8_t value);
	void write8output(uint8_t port, uint8_t value);
	uint8_t read8input(uint8_t port);
	uint8_t read8output(uint8_t port);
};

#endif //I8080_INPUTOUTPUT_H
