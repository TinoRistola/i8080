
#ifndef I8080_INSTRUCTION_H
#define I8080_INSTRUCTION_H

#include <cstdint>
#include <memory>
#include <algorithm>
#include <string>

using std::string;

class i8080;
class Instruction;

typedef void (i8080::*Func)(uint16_t);

class Instruction
{
	string  _mnemonic;
public:
	Instruction();
	Instruction(Func f, const char* mnemonic, uint8_t length, uint8_t cycles);

	Func func;

	uint8_t length;
	uint8_t cycles;

	string getMnemonic(uint16_t operand) const;
};

#endif //I8080_INSTRUCTION_H
