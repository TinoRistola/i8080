
#include "i8080.h"

i8080::i8080()
{
	_instr[0xCE] = Instruction(&i8080::ACI, "ACI %02XH", 2, 7);
	_instr[0x8F] = Instruction(&i8080::ADC, "ADC A", 1, 4);
	_instr[0x88] = Instruction(&i8080::ADC, "ADC B", 1, 4);
	_instr[0x89] = Instruction(&i8080::ADC, "ADC C", 1, 4);
	_instr[0x8A] = Instruction(&i8080::ADC, "ADC D", 1, 4);
	_instr[0x8B] = Instruction(&i8080::ADC, "ADC E", 1, 4);
	_instr[0x8C] = Instruction(&i8080::ADC, "ADC H", 1, 4);
	_instr[0x8D] = Instruction(&i8080::ADC, "ADC L", 1, 4);
	_instr[0x8E] = Instruction(&i8080::ADC, "ADC M", 1, 7);
	_instr[0x87] = Instruction(&i8080::ADD, "ADD A", 1, 4);
	_instr[0x80] = Instruction(&i8080::ADD, "ADD B", 1, 4);
	_instr[0x81] = Instruction(&i8080::ADD, "ADD C", 1, 4);
	_instr[0x82] = Instruction(&i8080::ADD, "ADD D", 1, 4);
	_instr[0x83] = Instruction(&i8080::ADD, "ADD E", 1, 4);
	_instr[0x84] = Instruction(&i8080::ADD, "ADD H", 1, 4);
	_instr[0x85] = Instruction(&i8080::ADD, "ADD L", 1, 4);
	_instr[0x86] = Instruction(&i8080::ADD, "ADD M", 1, 7);
	_instr[0xC6] = Instruction(&i8080::ADI, "ADI %02XH", 2, 7);
	_instr[0xA7] = Instruction(&i8080::ANA, "ANA A", 1, 4);
	_instr[0xA0] = Instruction(&i8080::ANA, "ANA B", 1, 4);
	_instr[0xA1] = Instruction(&i8080::ANA, "ANA C", 1, 4);
	_instr[0xA2] = Instruction(&i8080::ANA, "ANA D", 1, 4);
	_instr[0xA3] = Instruction(&i8080::ANA, "ANA E", 1, 4);
	_instr[0xA4] = Instruction(&i8080::ANA, "ANA H", 1, 4);
	_instr[0xA5] = Instruction(&i8080::ANA, "ANA L", 1, 4);
	_instr[0xA6] = Instruction(&i8080::ANA, "ANA M", 1, 7);
	_instr[0xE6] = Instruction(&i8080::ANI, "ANI %02XH", 2, 7);
	_instr[0xCD] = Instruction(&i8080::CALL, "CALL %04XH", 3, 17);
	_instr[0xDD] = Instruction(&i8080::CALL, "CALL %04XH", 3, 17);
	_instr[0xED] = Instruction(&i8080::CALL, "CALL %04XH", 3, 17);
	_instr[0xFD] = Instruction(&i8080::CALL, "CALL %04XH", 3, 17);
	_instr[0xDC] = Instruction(&i8080::CC, "CC %04XH", 3, 11);
	_instr[0xFC] = Instruction(&i8080::CM, "CM %04XH", 3, 11);
	_instr[0x2F] = Instruction(&i8080::CMA, "CMA", 1, 4);
	_instr[0x3F] = Instruction(&i8080::CMC, "CMC", 1, 4);
	_instr[0xBF] = Instruction(&i8080::CMP, "CMP A", 1, 4);
	_instr[0xB8] = Instruction(&i8080::CMP, "CMP B", 1, 4);
	_instr[0xB9] = Instruction(&i8080::CMP, "CMP C", 1, 4);
	_instr[0xBA] = Instruction(&i8080::CMP, "CMP D", 1, 4);
	_instr[0xBB] = Instruction(&i8080::CMP, "CMP E", 1, 4);
	_instr[0xBC] = Instruction(&i8080::CMP, "CMP H", 1, 4);
	_instr[0xBD] = Instruction(&i8080::CMP, "CMP L", 1, 4);
	_instr[0xBE] = Instruction(&i8080::CMP, "CMP M", 1, 7);
	_instr[0xD4] = Instruction(&i8080::CNC, "CNC %04XH", 3, 11);
	_instr[0xC4] = Instruction(&i8080::CNZ, "CNZ %04XH", 3, 11);
	_instr[0xF4] = Instruction(&i8080::CP, "CP %04XH", 3, 11);
	_instr[0xEC] = Instruction(&i8080::CPE, "CPE %04XH", 3, 11);
	_instr[0xFE] = Instruction(&i8080::CPI, "CPI %02XH", 2, 7);
	_instr[0xE4] = Instruction(&i8080::CPO, "CPO %04XH", 3, 11);
	_instr[0xCC] = Instruction(&i8080::CZ, "CZ %04XH", 3, 11);
	_instr[0x27] = Instruction(&i8080::DAA, "DAA", 1, 4);
	_instr[0x09] = Instruction(&i8080::DAD, "DAD B", 1, 10);
	_instr[0x19] = Instruction(&i8080::DAD, "DAD D", 1, 10);
	_instr[0x29] = Instruction(&i8080::DAD, "DAD H", 1, 10);
	_instr[0x39] = Instruction(&i8080::DAD, "DAD SP", 1, 10);
	_instr[0x3D] = Instruction(&i8080::DCR, "DCR A", 1, 5);
	_instr[0x05] = Instruction(&i8080::DCR, "DCR B", 1, 5);
	_instr[0x0D] = Instruction(&i8080::DCR, "DCR C", 1, 5);
	_instr[0x15] = Instruction(&i8080::DCR, "DCR D", 1, 5);
	_instr[0x1D] = Instruction(&i8080::DCR, "DCR E", 1, 5);
	_instr[0x25] = Instruction(&i8080::DCR, "DCR H", 1, 5);
	_instr[0x2D] = Instruction(&i8080::DCR, "DCR L", 1, 5);
	_instr[0x35] = Instruction(&i8080::DCR, "DCR M", 1, 10);
	_instr[0x0B] = Instruction(&i8080::DCX, "DCX B", 1, 5);
	_instr[0x1B] = Instruction(&i8080::DCX, "DCX D", 1, 5);
	_instr[0x2B] = Instruction(&i8080::DCX, "DCX H", 1, 5);
	_instr[0x3B] = Instruction(&i8080::DCX, "DCX SP", 1, 5);
	_instr[0xF3] = Instruction(&i8080::DI, "DI", 1, 4);
	_instr[0xFB] = Instruction(&i8080::EI, "EI", 1, 4);
	_instr[0x76] = Instruction(&i8080::HLT, "HLT", 1, 7);
	_instr[0xDB] = Instruction(&i8080::IN, "IN %02XH", 2, 10);
	_instr[0x3C] = Instruction(&i8080::INR, "INR A", 1, 5);
	_instr[0x04] = Instruction(&i8080::INR, "INR B", 1, 5);
	_instr[0x0C] = Instruction(&i8080::INR, "INR C", 1, 5);
	_instr[0x14] = Instruction(&i8080::INR, "INR D", 1, 5);
	_instr[0x1C] = Instruction(&i8080::INR, "INR E", 1, 5);
	_instr[0x24] = Instruction(&i8080::INR, "INR H", 1, 5);
	_instr[0x2C] = Instruction(&i8080::INR, "INR L", 1, 5);
	_instr[0x34] = Instruction(&i8080::INR, "INR M", 1, 10);
	_instr[0x03] = Instruction(&i8080::INX, "INX B", 1, 5);
	_instr[0x13] = Instruction(&i8080::INX, "INX D", 1, 5);
	_instr[0x23] = Instruction(&i8080::INX, "INX H", 1, 5);
	_instr[0x33] = Instruction(&i8080::INX, "INX SP", 1, 5);
	_instr[0xDA] = Instruction(&i8080::JC, "JC %04XH", 3, 10);
	_instr[0xFA] = Instruction(&i8080::JM, "JM %04XH", 3, 10);
	_instr[0xC3] = Instruction(&i8080::JMP, "JMP %04XH", 3, 10);
	_instr[0xCB] = Instruction(&i8080::JMP, "JMP %04XH", 3, 10);
	_instr[0xD2] = Instruction(&i8080::JNC, "JNC %04XH", 3, 10);
	_instr[0xC2] = Instruction(&i8080::JNZ, "JNZ %04XH", 3, 10);
	_instr[0xF2] = Instruction(&i8080::JP, "JP %04XH", 3, 10);
	_instr[0xEA] = Instruction(&i8080::JPE, "JPE %04XH", 3, 10);
	_instr[0xE2] = Instruction(&i8080::JPO, "JPO %04XH", 3, 10);
	_instr[0xCA] = Instruction(&i8080::JZ, "JZ %04XH", 3, 10);
	_instr[0x3A] = Instruction(&i8080::LDA, "LDA %04XH", 3, 13);
	_instr[0x0A] = Instruction(&i8080::LDAX, "LDAX B", 1, 7);
	_instr[0x1A] = Instruction(&i8080::LDAX, "LDAX D", 1, 7);
	_instr[0x2A] = Instruction(&i8080::LHLD, "LHLD %04XH", 3, 16);
	_instr[0x01] = Instruction(&i8080::LXI, "LXI B,%04XH", 3, 10);
	_instr[0x11] = Instruction(&i8080::LXI, "LXI D,%04XH", 3, 10);
	_instr[0x21] = Instruction(&i8080::LXI, "LXI H,%04XH", 3, 10);
	_instr[0x31] = Instruction(&i8080::LXI, "LXI SP,%04XH", 3, 10);
	_instr[0x7F] = Instruction(&i8080::MOV, "MOV A,A", 1, 5);
	_instr[0x78] = Instruction(&i8080::MOV, "MOV A,B", 1, 5);
	_instr[0x79] = Instruction(&i8080::MOV, "MOV A,C", 1, 5);
	_instr[0x7A] = Instruction(&i8080::MOV, "MOV A,D", 1, 5);
	_instr[0x7B] = Instruction(&i8080::MOV, "MOV A,E", 1, 5);
	_instr[0x7C] = Instruction(&i8080::MOV, "MOV A,H", 1, 5);
	_instr[0x7D] = Instruction(&i8080::MOV, "MOV A,L", 1, 5);
	_instr[0x7E] = Instruction(&i8080::MOV, "MOV A,M", 1, 7);
	_instr[0x47] = Instruction(&i8080::MOV, "MOV B,A", 1, 5);
	_instr[0x40] = Instruction(&i8080::MOV, "MOV B,B", 1, 5);
	_instr[0x41] = Instruction(&i8080::MOV, "MOV B,C", 1, 5);
	_instr[0x42] = Instruction(&i8080::MOV, "MOV B,D", 1, 5);
	_instr[0x43] = Instruction(&i8080::MOV, "MOV B,E", 1, 5);
	_instr[0x44] = Instruction(&i8080::MOV, "MOV B,H", 1, 5);
	_instr[0x45] = Instruction(&i8080::MOV, "MOV B,L", 1, 5);
	_instr[0x46] = Instruction(&i8080::MOV, "MOV B,M", 1, 7);
	_instr[0x4F] = Instruction(&i8080::MOV, "MOV C,A", 1, 5);
	_instr[0x48] = Instruction(&i8080::MOV, "MOV C,B", 1, 5);
	_instr[0x49] = Instruction(&i8080::MOV, "MOV C,C", 1, 5);
	_instr[0x4A] = Instruction(&i8080::MOV, "MOV C,D", 1, 5);
	_instr[0x4B] = Instruction(&i8080::MOV, "MOV C,E", 1, 5);
	_instr[0x4C] = Instruction(&i8080::MOV, "MOV C,H", 1, 5);
	_instr[0x4D] = Instruction(&i8080::MOV, "MOV C,L", 1, 5);
	_instr[0x4E] = Instruction(&i8080::MOV, "MOV C,M", 1, 7);
	_instr[0x57] = Instruction(&i8080::MOV, "MOV D,A", 1, 5);
	_instr[0x50] = Instruction(&i8080::MOV, "MOV D,B", 1, 5);
	_instr[0x51] = Instruction(&i8080::MOV, "MOV D,C", 1, 5);
	_instr[0x52] = Instruction(&i8080::MOV, "MOV D,D", 1, 5);
	_instr[0x53] = Instruction(&i8080::MOV, "MOV D,E", 1, 5);
	_instr[0x54] = Instruction(&i8080::MOV, "MOV D,H", 1, 5);
	_instr[0x55] = Instruction(&i8080::MOV, "MOV D,L", 1, 5);
	_instr[0x56] = Instruction(&i8080::MOV, "MOV D,M", 1, 7);
	_instr[0x5F] = Instruction(&i8080::MOV, "MOV E,A", 1, 5);
	_instr[0x58] = Instruction(&i8080::MOV, "MOV E,B", 1, 5);
	_instr[0x59] = Instruction(&i8080::MOV, "MOV E,C", 1, 5);
	_instr[0x5A] = Instruction(&i8080::MOV, "MOV E,D", 1, 5);
	_instr[0x5B] = Instruction(&i8080::MOV, "MOV E,E", 1, 5);
	_instr[0x5C] = Instruction(&i8080::MOV, "MOV E,H", 1, 5);
	_instr[0x5D] = Instruction(&i8080::MOV, "MOV E,L", 1, 5);
	_instr[0x5E] = Instruction(&i8080::MOV, "MOV E,M", 1, 7);
	_instr[0x67] = Instruction(&i8080::MOV, "MOV H,A", 1, 5);
	_instr[0x60] = Instruction(&i8080::MOV, "MOV H,B", 1, 5);
	_instr[0x61] = Instruction(&i8080::MOV, "MOV H,C", 1, 5);
	_instr[0x62] = Instruction(&i8080::MOV, "MOV H,D", 1, 5);
	_instr[0x63] = Instruction(&i8080::MOV, "MOV H,E", 1, 5);
	_instr[0x64] = Instruction(&i8080::MOV, "MOV H,H", 1, 5);
	_instr[0x65] = Instruction(&i8080::MOV, "MOV H,L", 1, 5);
	_instr[0x66] = Instruction(&i8080::MOV, "MOV H,M", 1, 7);
	_instr[0x6F] = Instruction(&i8080::MOV, "MOV L,A", 1, 5);
	_instr[0x68] = Instruction(&i8080::MOV, "MOV L,B", 1, 5);
	_instr[0x69] = Instruction(&i8080::MOV, "MOV L,C", 1, 5);
	_instr[0x6A] = Instruction(&i8080::MOV, "MOV L,D", 1, 5);
	_instr[0x6B] = Instruction(&i8080::MOV, "MOV L,E", 1, 5);
	_instr[0x6C] = Instruction(&i8080::MOV, "MOV L,H", 1, 5);
	_instr[0x6D] = Instruction(&i8080::MOV, "MOV L,L", 1, 5);
	_instr[0x6E] = Instruction(&i8080::MOV, "MOV L,M", 1, 7);
	_instr[0x77] = Instruction(&i8080::MOV, "MOV M,A", 1, 7);
	_instr[0x70] = Instruction(&i8080::MOV, "MOV M,B", 1, 7);
	_instr[0x71] = Instruction(&i8080::MOV, "MOV M,C", 1, 7);
	_instr[0x72] = Instruction(&i8080::MOV, "MOV M,D", 1, 7);
	_instr[0x73] = Instruction(&i8080::MOV, "MOV M,E", 1, 7);
	_instr[0x74] = Instruction(&i8080::MOV, "MOV M,H", 1, 7);
	_instr[0x75] = Instruction(&i8080::MOV, "MOV M,L", 1, 7);
	_instr[0x3E] = Instruction(&i8080::MVI, "MVI A,%02XH", 2, 7);
	_instr[0x06] = Instruction(&i8080::MVI, "MVI B,%02XH", 2, 7);
	_instr[0x0E] = Instruction(&i8080::MVI, "MVI C,%02XH", 2, 7);
	_instr[0x16] = Instruction(&i8080::MVI, "MVI D,%02XH", 2, 7);
	_instr[0x1E] = Instruction(&i8080::MVI, "MVI E,%02XH", 2, 7);
	_instr[0x26] = Instruction(&i8080::MVI, "MVI H,%02XH", 2, 7);
	_instr[0x2E] = Instruction(&i8080::MVI, "MVI L,%02XH", 2, 7);
	_instr[0x36] = Instruction(&i8080::MVI, "MVI M,%02XH", 2, 10);
	_instr[0x00] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x10] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x20] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x30] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x08] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x18] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x28] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0x38] = Instruction(&i8080::NOP, "NOP", 1, 4);
	_instr[0xB7] = Instruction(&i8080::ORA, "ORA A", 1, 4);
	_instr[0xB0] = Instruction(&i8080::ORA, "ORA B", 1, 4);
	_instr[0xB1] = Instruction(&i8080::ORA, "ORA C", 1, 4);
	_instr[0xB2] = Instruction(&i8080::ORA, "ORA D", 1, 4);
	_instr[0xB3] = Instruction(&i8080::ORA, "ORA E", 1, 4);
	_instr[0xB4] = Instruction(&i8080::ORA, "ORA H", 1, 4);
	_instr[0xB5] = Instruction(&i8080::ORA, "ORA L", 1, 4);
	_instr[0xB6] = Instruction(&i8080::ORA, "ORA M", 1, 7);
	_instr[0xF6] = Instruction(&i8080::ORI, "ORI %02XH", 2, 7);
	_instr[0xD3] = Instruction(&i8080::OUT, "OUT %02XH", 2, 10);
	_instr[0xE9] = Instruction(&i8080::PCHL, "PCHL", 1, 5);
	_instr[0xC1] = Instruction(&i8080::POP, "POP B", 1, 10);
	_instr[0xD1] = Instruction(&i8080::POP, "POP D", 1, 10);
	_instr[0xE1] = Instruction(&i8080::POP, "POP H", 1, 10);
	_instr[0xF1] = Instruction(&i8080::POP, "POP PSW", 1, 10);
	_instr[0xC5] = Instruction(&i8080::PUSH, "PUSH B", 1, 11);
	_instr[0xD5] = Instruction(&i8080::PUSH, "PUSH D", 1, 11);
	_instr[0xE5] = Instruction(&i8080::PUSH, "PUSH H", 1, 11);
	_instr[0xF5] = Instruction(&i8080::PUSH, "PUSH PSW", 1, 11);
	_instr[0x17] = Instruction(&i8080::RAL, "RAL", 1, 4);
	_instr[0x1F] = Instruction(&i8080::RAR, "RAR", 1, 4);
	_instr[0xD8] = Instruction(&i8080::RC, "RC", 1, 5);
	_instr[0xC9] = Instruction(&i8080::RET, "RET", 1, 10);
	_instr[0xD9] = Instruction(&i8080::RET, "RET", 1, 10);
	_instr[0x07] = Instruction(&i8080::RLC, "RLC", 1, 4);
	_instr[0xF8] = Instruction(&i8080::RM, "RM", 1, 5);
	_instr[0xD0] = Instruction(&i8080::RNC, "RNC", 1, 5);
	_instr[0xC0] = Instruction(&i8080::RNZ, "RNZ", 1, 5);
	_instr[0xF0] = Instruction(&i8080::RP, "RP", 1, 5);
	_instr[0xE8] = Instruction(&i8080::RPE, "RPE", 1, 5);
	_instr[0xE0] = Instruction(&i8080::RPO, "RPO", 1, 5);
	_instr[0x0F] = Instruction(&i8080::RRC, "RRC", 1, 4);
	_instr[0xC7] = Instruction(&i8080::RST, "RST 0", 1, 11);
	_instr[0xCF] = Instruction(&i8080::RST, "RST 1", 1, 11);
	_instr[0xD7] = Instruction(&i8080::RST, "RST 2", 1, 11);
	_instr[0xDF] = Instruction(&i8080::RST, "RST 3", 1, 11);
	_instr[0xE7] = Instruction(&i8080::RST, "RST 4", 1, 11);
	_instr[0xEF] = Instruction(&i8080::RST, "RST 5", 1, 11);
	_instr[0xF7] = Instruction(&i8080::RST, "RST 6", 1, 11);
	_instr[0xFF] = Instruction(&i8080::RST, "RST 7", 1, 11);
	_instr[0xC8] = Instruction(&i8080::RZ, "RZ", 1, 5);
	_instr[0x9F] = Instruction(&i8080::SBB, "SBB A", 1, 4);
	_instr[0x98] = Instruction(&i8080::SBB, "SBB B", 1, 4);
	_instr[0x99] = Instruction(&i8080::SBB, "SBB C", 1, 4);
	_instr[0x9A] = Instruction(&i8080::SBB, "SBB D", 1, 4);
	_instr[0x9B] = Instruction(&i8080::SBB, "SBB E", 1, 4);
	_instr[0x9C] = Instruction(&i8080::SBB, "SBB H", 1, 4);
	_instr[0x9D] = Instruction(&i8080::SBB, "SBB L", 1, 4);
	_instr[0x9E] = Instruction(&i8080::SBB, "SBB M", 1, 7);
	_instr[0xDE] = Instruction(&i8080::SBI, "SBI %02XH", 2, 7);
	_instr[0x22] = Instruction(&i8080::SHLD, "SHLD %04XH", 3, 16);
	_instr[0xF9] = Instruction(&i8080::SPHL, "SPHL", 1, 5);
	_instr[0x32] = Instruction(&i8080::STA, "STA %04XH", 3, 13);
	_instr[0x02] = Instruction(&i8080::STAX, "STAX B", 1, 7);
	_instr[0x12] = Instruction(&i8080::STAX, "STAX D", 1, 7);
	_instr[0x37] = Instruction(&i8080::STC, "STC", 1, 4);
	_instr[0x97] = Instruction(&i8080::SUB, "SUB A", 1, 4);
	_instr[0x90] = Instruction(&i8080::SUB, "SUB B", 1, 4);
	_instr[0x91] = Instruction(&i8080::SUB, "SUB C", 1, 4);
	_instr[0x92] = Instruction(&i8080::SUB, "SUB D", 1, 4);
	_instr[0x93] = Instruction(&i8080::SUB, "SUB E", 1, 4);
	_instr[0x94] = Instruction(&i8080::SUB, "SUB H", 1, 4);
	_instr[0x95] = Instruction(&i8080::SUB, "SUB L", 1, 4);
	_instr[0x96] = Instruction(&i8080::SUB, "SUB M", 1, 7);
	_instr[0xD6] = Instruction(&i8080::SUI, "SUI %02XH", 2, 7);
	_instr[0xEB] = Instruction(&i8080::XCHG, "XCHG", 1, 4);
	_instr[0xAF] = Instruction(&i8080::XRA, "XRA A", 1, 4);
	_instr[0xA8] = Instruction(&i8080::XRA, "XRA B", 1, 4);
	_instr[0xA9] = Instruction(&i8080::XRA, "XRA C", 1, 4);
	_instr[0xAA] = Instruction(&i8080::XRA, "XRA D", 1, 4);
	_instr[0xAB] = Instruction(&i8080::XRA, "XRA E", 1, 4);
	_instr[0xAC] = Instruction(&i8080::XRA, "XRA H", 1, 4);
	_instr[0xAD] = Instruction(&i8080::XRA, "XRA L", 1, 4);
	_instr[0xAE] = Instruction(&i8080::XRA, "XRA M", 1, 7);
	_instr[0xEE] = Instruction(&i8080::XRI, "XRI %02XH", 2, 7);
	_instr[0xE3] = Instruction(&i8080::XTHL, "XTHL", 1, 18);

	reset();
}

void i8080::reset()
{
	_cycles = 0;
	reg.reset();
}

void i8080::step()
{
	if (_cycles > 0)
	{
		_cycles--;
		return;
	}

	const uint8_t opcode = ram.read8(reg.PC);
	const Instruction* i = &_instr[opcode];

	if (i && i->func)
	{
		uint16_t operand = 0;

		if (i->length == 1)
			operand = opcode;
		else if (i->length == 2)
			operand = ram.read8(reg.PC + 1);
		else
			operand = ram.read16(reg.PC + 1);

		reg.PC += i->length;
		_cycles = i->cycles;

		_currentOpcode = opcode;

		(this->*i->func)(operand);
	}
	else
	{
		std::cerr << "Opcode 0x" << std::hex << static_cast<uint16_t>(opcode) << std::dec << " not implemented"
				  << std::endl;
		exit(1);
	}
}

void i8080::interrupt(uint16_t addr)
{
	if(_enableInterrupts)
	{
		_enableInterrupts = false;
		call(addr);
		_cycles += 11;
	}
}

void i8080::load(const char* fileName, uint16_t offset)
{
	std::ifstream ifs(fileName, std::ios::binary | std::ios::ate);
	auto pos = ifs.tellg();

	ifs.seekg(0, std::ios::beg);
	ifs.read(reinterpret_cast<char*>(ram.get() + offset), pos);
	ifs.close();
}

void i8080::push16(uint16_t value)
{
	reg.SP -= 2;
	ram.write16(reg.SP, value);
}

uint16_t i8080::pop16()
{
	uint16_t result = ram.read16(reg.SP);
	reg.SP += 2;
	return result;
}

uint8_t* i8080::getDST()
{
	return getRegister((uint8_t)((_currentOpcode & 0b00111000) >> 3));
}

uint8_t* i8080::getSRC()
{
	return getRegister((uint8_t)(_currentOpcode & 0b00000111));
}

uint8_t* i8080::getRegister(uint8_t _reg)
{
	auto ptr = reg.getRegister(_reg);

	if (!ptr)
		ptr = ram.read8ptr(reg.getPair(H));

	return ptr;
}

void i8080::call(uint16_t operand)
{
	push16(reg.PC);
	reg.PC = operand;
}

void i8080::ret()
{
	reg.PC = pop16();
}

void i8080::ACI(uint16_t operand)
{
	utils::add(reg, &reg.A, static_cast<uint8_t>(operand), reg.getSR(Carry));
}

void i8080::ADC(uint16_t operand)
{
	auto src = getSRC();

	utils::add(reg, &reg.A, *src, reg.getSR(Carry));
}

void i8080::ADD(uint16_t operand)
{
	auto src = getSRC();

	utils::add(reg, &reg.A, *src);
}

void i8080::ADI(uint16_t operand)
{
	utils::add(reg, &reg.A, static_cast<uint8_t>(operand));
}

void i8080::ANA(uint16_t operand)
{
	auto src = getSRC();

	const uint16_t result = reg.A & *src;

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, ((reg.A | *src) & 0x8) != 0);
	reg.setSR(result, Sign, Zero, Parity);

	reg.A = static_cast<uint8_t>(result);
}

void i8080::ANI(uint16_t operand)
{
	const auto value = static_cast<uint8_t>(operand);
	const uint16_t result = reg.A & value;

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, ((reg.A | value) & 0x8) != 0);
	reg.setSR(result, Sign, Zero, Parity);

	reg.A = static_cast<uint8_t>(result);
}

void i8080::CALL(uint16_t operand)
{
	call(operand);
}

void i8080::CC(uint16_t operand)
{
	if (reg.getSR(Carry))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CM(uint16_t operand)
{
	if (reg.getSR(Sign))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CMA(uint16_t operand)
{
	reg.A ^= 0xFF;
}

void i8080::CMC(uint16_t operand)
{
	reg.setSR(Carry, !reg.getSR(Carry));
}

void i8080::CMP(uint16_t operand)
{
	auto src = getSRC();

	utils::compare(reg, reg.A, *src);
}

void i8080::CNC(uint16_t operand)
{
	if (!reg.getSR(Carry))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CNZ(uint16_t operand)
{
	if (!reg.getSR(Zero))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CP(uint16_t operand)
{
	if (!reg.getSR(Sign))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CPE(uint16_t operand)
{
	if (reg.getSR(Parity))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CPI(uint16_t operand)
{
	utils::compare(reg, reg.A, static_cast<uint8_t>(operand));
}

void i8080::CPO(uint16_t operand)
{
	if (!reg.getSR(Parity))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::CZ(uint16_t operand)
{
	if (reg.getSR(Zero))
	{
		call(operand);
		_cycles += 6;
	}
}

void i8080::DAA(uint16_t operand)
{
	uint16_t result = 0;
	bool carry = reg.getSR(Carry);

	if(reg.getSR(Auxiliary) || (reg.A & 0x0F) > 9)
		result = 6;

	if(reg.getSR(Carry) || (reg.A >> 4) > 9 || ((reg.A >> 4) >= 9 && (reg.A & 0x0F) > 9))
	{
		result += 0x60;
		carry = true;
	}

	ADI(result);
	reg.setSR(Carry, carry);
}

void i8080::DAD(uint16_t operand)
{
	auto pair = static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4);
	auto data = reg.getPair(pair);

	if (pair == PSW)
		data = reg.SP;

	uint32_t result = static_cast<uint32_t>(reg.getPair(H)) + static_cast<uint32_t>(data);

	reg.setSR(Carry, (result & 0x10000) != 0);
	reg.setPair(H, static_cast<uint16_t>(result & 0xFFFF));
}

void i8080::DCR(uint16_t operand)
{
	auto dst = getDST();

	uint16_t result = static_cast<uint16_t>(*dst) - 1;

	reg.setSR(Auxiliary, (result & 0x0F) != 0x0F);
	reg.setSR(result, Sign, Zero, Parity);

	*dst = static_cast<uint8_t>(result & 0xFF);
}

void i8080::DCX(uint16_t operand)
{
	auto pair = static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4);
	auto data = reg.getPair(pair);

	if (pair == PSW)
		data = reg.SP;

	data--;

	if (pair == PSW)
		reg.SP = data;
	else
		reg.setPair(pair, data);
}

void i8080::DI(uint16_t operand)
{
	_enableInterrupts = false;
}

void i8080::EI(uint16_t operand)
{
	_enableInterrupts = true;
}

void i8080::HLT(uint16_t operand)
{
	reg.PC--;
}

void i8080::IN(uint16_t operand)
{
	reg.A = io.read8input(static_cast<uint8_t>(operand & 0xFF));
}

void i8080::INR(uint16_t operand)
{
	auto dst = getDST();

	uint16_t result = static_cast<uint16_t>(*dst) + 1;

	reg.setSR(Auxiliary, (result & 0x0F) == 0);
	reg.setSR(result, Sign, Zero, Parity);

	*dst = static_cast<uint8_t>(result & 0xFF);
}

void i8080::INX(uint16_t operand)
{
	auto pair = static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4);
	auto data = reg.getPair(pair);

	if (pair == PSW)
		data = reg.SP;

	data++;

	if (pair == PSW)
		reg.SP = data;
	else
		reg.setPair(pair, data);
}

void i8080::JC(uint16_t operand)
{
	if (reg.getSR(Carry))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JM(uint16_t operand)
{
	if (reg.getSR(Sign))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JMP(uint16_t operand)
{
	reg.PC = operand;
}

void i8080::JNC(uint16_t operand)
{
	if (!reg.getSR(Carry))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JNZ(uint16_t operand)
{
	if (!reg.getSR(Zero))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JP(uint16_t operand)
{
	if (!reg.getSR(Sign))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JPE(uint16_t operand)
{
	if (reg.getSR(Parity))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JPO(uint16_t operand)
{
	if (!reg.getSR(Parity))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::JZ(uint16_t operand)
{
	if (reg.getSR(Zero))
	{
		reg.PC = operand;
		_cycles += 6;
	}
}

void i8080::LDA(uint16_t operand)
{
	reg.A = ram.read8(operand);
}

void i8080::LDAX(uint16_t operand)
{
	reg.A = ram.read8(reg.getPair((_currentOpcode & 0b00010000) ? D : B));
}

void i8080::LHLD(uint16_t operand)
{
	reg.setPair(H, ram.read16(operand));
}

void i8080::LXI(uint16_t operand)
{
	auto pair = static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4);

	if (pair == PSW)
		reg.SP = operand;
	else
		reg.setPair(pair, operand);
}

void i8080::MOV(uint16_t operand)
{
	auto dst = getDST();
	auto src = getSRC();

	*dst = *src;
}

void i8080::MVI(uint16_t operand)
{
	auto dst = getDST();

	*dst = static_cast<uint8_t>(operand);
}

void i8080::NOP(uint16_t operand)
{
}

void i8080::ORA(uint16_t operand)
{
	reg.A |= *getSRC();

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, false);
	reg.setSR(reg.A, Sign, Zero, Parity);
}

void i8080::ORI(uint16_t operand)
{
	reg.A |= static_cast<uint8_t>(operand);

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, false);
	reg.setSR(reg.A, Sign, Zero, Parity);
}

void i8080::OUT(uint16_t operand)
{
	io.write8output(static_cast<uint8_t>(operand & 0xFF), reg.A);
}

void i8080::PCHL(uint16_t operand)
{
	reg.PC = reg.getPair(H);
}

void i8080::POP(uint16_t operand)
{
	auto pair = static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4);

	reg.setPair(pair, pop16());
}

void i8080::PUSH(uint16_t operand)
{
	auto data = reg.getPair(static_cast<enumRegisterPairs>((_currentOpcode & 0b00110000) >> 4));

	push16(data);
}

void i8080::RAL(uint16_t operand)
{
	const bool carry = reg.getSR(Carry);
	reg.setSR(Carry, (reg.A >> 7) != 0);
	reg.A = (reg.A << 1) | carry;
}

void i8080::RAR(uint16_t operand)
{
	const bool carry = reg.getSR(Carry);
	reg.setSR(Carry, (reg.A & 1) != 0);
	reg.A = (reg.A >> 1) | (carry << 7);
}

void i8080::RC(uint16_t operand)
{
	if (reg.getSR(Carry))
		ret();
}

void i8080::RET(uint16_t operand)
{
	ret();
}

void i8080::RLC(uint16_t operand)
{
	reg.setSR(Carry, reg.A >> 7);
	reg.A = (reg.A << 1) | reg.getSR(Carry);
}

void i8080::RM(uint16_t operand)
{
	if (reg.getSR(Sign))
		ret();
}

void i8080::RNC(uint16_t operand)
{
	if (!reg.getSR(Carry))
		ret();
}

void i8080::RNZ(uint16_t operand)
{
	if (!reg.getSR(Zero))
		ret();
}

void i8080::RP(uint16_t operand)
{
	if (!reg.getSR(Sign))
		ret();
}

void i8080::RPE(uint16_t operand)
{
	if (reg.getSR(Parity))
		ret();
}

void i8080::RPO(uint16_t operand)
{
	if (!reg.getSR(Parity))
		ret();
}

void i8080::RRC(uint16_t operand)
{
	reg.setSR(Carry, (reg.A & 1) != 0);
	reg.A = (reg.A >> 1) | (reg.getSR() << 7);
}

void i8080::RST(uint16_t operand)
{
	call((operand & 0b00111000) >> 3);
}

void i8080::RZ(uint16_t operand)
{
	if (reg.getSR(Zero))
		ret();
}

void i8080::SBB(uint16_t operand)
{
	auto src = getSRC();

	utils::subtract(reg, &reg.A, *src, reg.getSR(Carry));
}

void i8080::SBI(uint16_t operand)
{
	utils::subtract(reg, &reg.A, static_cast<uint8_t>(operand), reg.getSR(Carry));
}

void i8080::SHLD(uint16_t operand)
{
	ram.write16(operand, reg.getPair(H));
}

void i8080::SPHL(uint16_t operand)
{
	reg.SP = reg.getPair(H);
}

void i8080::STA(uint16_t operand)
{
	ram.write16(operand, reg.A);
}

void i8080::STAX(uint16_t operand)
{
	ram.write8(reg.getPair((_currentOpcode & 0b00010000) ? D : B), reg.A);
}

void i8080::STC(uint16_t operand)
{
	reg.setSR(Carry, true);
}

void i8080::SUB(uint16_t operand)
{
	auto src = getSRC();

	utils::subtract(reg, &reg.A, *src);
}

void i8080::SUI(uint16_t operand)
{
	utils::subtract(reg, &reg.A, static_cast<uint8_t>(operand));
}

void i8080::XCHG(uint16_t operand)
{
	auto de = reg.getPair(D);
	auto hl = reg.getPair(H);

	reg.setPair(D, hl);
	reg.setPair(H, de);
}

void i8080::XRA(uint16_t operand)
{
	auto src = getSRC();

	reg.A ^= *src;

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, false);
	reg.setSR(reg.A, Sign, Zero, Parity);
}

void i8080::XRI(uint16_t operand)
{
	reg.A ^= static_cast<uint8_t>(operand);

	reg.setSR(Carry, false);
	reg.setSR(Auxiliary, false);
	reg.setSR(reg.A, Sign, Zero, Parity);
}

void i8080::XTHL(uint16_t operand)
{
	auto addr = ram.read16(reg.SP);
	auto hl   = reg.getPair(H);

	ram.write16(reg.SP, hl);
	reg.setPair(H, addr);
}
