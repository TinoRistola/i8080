
#ifndef I8080_UTILS_H
#define I8080_UTILS_H

#include <cstdint>

class Registers;
namespace utils
{
	void subtract(Registers& reg, uint8_t* dst, uint8_t x, bool carry = false);

	void add(Registers& reg, uint8_t* dst, uint8_t x, bool carry = false);

	void compare(Registers& reg, uint8_t x, uint8_t y);

	bool parity(uint8_t x);
}

#endif //I8080_UTILS_H
